var schema = {
    type: 'object',
    properties: {
        users: {
            type: 'array',
            minItems: 60000,
            maxItems: 100000,
            items: {
                type: 'object',
                properties: {
                    id: {
                        type: 'number',
                        unique: true,
                        minimum: 1
                    },
                    firstName: {
                        type: 'string',
                        faker: 'name.firstName'
                    },
                    lastName: {
                        type: 'string',
                        faker: 'name.lastName'
                    },
                    email: {
                        type: 'string',
                        faker: 'internet.email'
                    },
                    specialDate: {
                        type: 'date',
                        faker: 'date.past'
                    },
                    url: {
                        type: 'string',
                        faker: 'internet.url'
                    },
                    married: {
                        type: 'boolean',
                        faker: 'random.boolean'
                    },
                    ammount: {
                        type: 'number',
                        faker: 'random.alphaNumeric'
                    },
                    phone: {
                        type: 'string',
                        faker: 'phone.phoneNumber'
                    },
                    description: {
                        type: 'string',
                        faker: 'lorem.paragraphs'
                    }
                },
                required: ['id', 'type', 'lastname', 'email']
            }
        }
    },
    required: ['users']
};

module.exports = schema;
