var schema = {
    type: 'object',
    properties: {
        rows: {
            type: 'array',
            minItems: 60000,
            maxItems: 100000,
            items: {
                type: 'object',
                properties: {
                    id: {
                        type: 'number',
                        unique: true,
                        minimum: 1
                    },
                    'String Identifier': {
                        type: 'string',
                        faker: 'name.firstName'
                    },
                    'Source Language Text': {
                        type: 'string',
                        faker: 'name.lastName'
                    },
                    StringTypeMain: {
                        type: 'string',
                        faker: 'address.streetAddress'
                    }
                },
                required: [
                    'id',
                    'String Identifier',
                    'Source Language Text',
                    'StringTypeMain'
                ]
            }
        }
    },
    required: ['rows']
};

module.exports = schema;
